<?php

class FormaPagamento extends AppModel {
	var $name='FormaPagamento';
	var $belongsTo = array(
		'Conta' => array(
			'className' => 'Conta',
			'foreignKey' => 'conta_principal'
		),
		'TipoDocumento' => array(
		    'className' => 'TipoDocumento',
		    'foreignKey' => 'tipo_documento_id'
		),
	);
	var $hasMany = array(
		'ServicoOrdem' => array(
			'className' => 'ServicoOrdem'
		),
		'PedidoVenda' => array(
			'className' => 'PedidoVenda'
		)
	);
	var $validate = array(
		'nome' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'numero_maximo_parcelas' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'numero_parcelas_sem_juros' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'dias_intervalo_parcelamento' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'porcentagem_juros' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'valor_minimo_parcela' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'porcentagem_desconto_a_vista' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'conta_principal' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
		'tipo_documento_id' => array(
			'rule' => 'notEmpty',
			'message' => 'Campo obrigatório.'
		),
	);
}

?>